<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Noticia
 *
 * @author Guilherme
 */
class Noticia {
    //put your code here
    
    private $corpo;
    private $data;
    private $titulo;
    
    function __construct($corpo, $data, $titulo) {
        $this->corpo = $corpo;
        $this->data = $data;
        $this->titulo = $titulo;
    }
    
    function getCorpo() {
        return $this->corpo;
    }

    function getData() {
        return $this->data;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function setCorpo($corpo) {
        $this->corpo = $corpo;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }



}
