<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Administrador
 *
 * @author Guilherme
 */
class Administrador {
    //put your code here
    
    private $login;
    private $nome;
    private $senha;
    
    function __construct($login, $nome, $senha) {
        $this->login = $login;
        $this->nome = $nome;
        $this->senha = $senha;
    }

    function getLogin() {
        return $this->login;
    }

    function getNome() {
        return $this->nome;
    }

    function getSenha() {
        return $this->senha;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }


}
