<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Imagem
 *
 * @author Guilherme
 */
class Imagem {
    //put your code here
    private $textoAlternativo;
    
    function __construct($textoAlternativo) {
        $this->textoAlternativo = $textoAlternativo;
    }
    
    function getTextoAlternativo() {
        return $this->textoAlternativo;
    }

    function setTextoAlternativo($textoAlternativo) {
        $this->textoAlternativo = $textoAlternativo;
    }

}
