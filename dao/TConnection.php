<?php

/**
 * Gerencia conex�o com banco de dados atrav�s do arquivo de configura��o
 **/

class TConnection {

    private function __construct() {
    }

    /**
	 * Recebe o nome do banco de dados e inst�ncia de objeto PDO correspondente
	**/

    public static function open($name) {

        if (file_exists("{$name}.ini")) {
            $db = parse_ini_file("{$name}.ini");
        } else {
            throw new Exception("Arquivo '$name' n�o encontrado");
        }

        $user = $db['user'];
        $password = $db['password'];
        $database = $db['database'];
        $host = $db['host'];

        $connect = mysql_connect($host, $user, $password);
        mysql_select_db($database);
        return $connect;
    }
}
?>